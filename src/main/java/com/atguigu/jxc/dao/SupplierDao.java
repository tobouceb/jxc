package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface SupplierDao {

    /**
     * 新增供应商方法
     */
    void updateSupplier(
            @Param("supplierId") Integer supplierId,
            @Param("supplier") Supplier supplier
    ) ;
    /**
     * 保存供应商方法
     */
    int saveSupplier(@Param("supplier") Supplier supplier) ;

    /**
     * 分页查询供应商方法
     */
    List<Supplier> getSupplierPage(
            @Param("page") Integer page,
            @Param("rows") Integer rows,
            @Param("supplierName") String supplierName);

    /**
     * 删除供应商方法
     */
    void deleteSupplier(List<String> ids);

}
