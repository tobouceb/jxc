package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    public List<Customer> getCustomerPage(
            @Param("page") int i,
            @Param("rows") Integer rows,
            @Param("customerName") String customerName);

    int saveCustomer(@Param("customer") Customer customer) ;


    void updateCustomer(
            @Param("customerId") Integer customerId,
            @Param("customer") Customer customer
    );

    void delete(List<String> ids);
}
