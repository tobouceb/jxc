package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List< Goods> getGoodsInventoryPage(
            @Param("page") Integer page,
            @Param("rows") Integer rows,
            @Param("codeOrName")String codeOrName,
            @Param("goodsTypeId")Integer goodsTypeId);

    List< Goods> getGoodsPage(
            @Param("page") Integer page,
            @Param("rows") Integer rows,
            @Param("goodsName")String goodsName,
            @Param("goodsTypeId")Integer goodsTypeId);

    void saveorupdate(Goods goods);

    Goods getGoodsById(@Param("goodsId")Integer goodsId);

    void delete(@Param("goodsId")Integer goodsId);

    /**
     * ^ 表示匹配字符串的开始位置。
     * \\d 表示匹配任意数字字符（等价于[0-9]）。
     * + 表示匹配前面的字符一次或多次。
     * $ 表示匹配字符串的结束位置。
     */
    List<Goods> getNoInventoryQuantity(@Param("nameOrCode")String nameOrCode);
    List<Goods> getHasInventoryQuantity(@Param("nameOrCode")String nameOrCode);


    /**
     * 在MyBatis中，当使用<if>标签进行条件判断时，如果条件表达式的值为0，会被视为false，不会执行<if>标签内的内容。
     * 使用trim()函数去除字符串两端的空格，然后判断是否为空字符串。这样即使传入的值为0，也会被视为非空字符串，条件判断会生效
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    void saveStock(
            @Param("goodsId")Integer goodsId,
            @Param("inventoryQuantity")int inventoryQuantity,
            @Param("purchasingPrice")double purchasingPrice);
}
