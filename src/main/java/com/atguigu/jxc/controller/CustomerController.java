package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.GoodsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @description 客户信息Controller
 */
@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param customerName yonghu 名称

     * @return
     */
    @PostMapping("/customer/list")
    //Integer page, Integer rows, String  customerName
    public Map<String,Object> listInventory(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "rows", required = false) Integer rows,
            @RequestParam(value = "codeOrName",required = false) String customerName

    ) {
        return customerService.getcustomerPage(page,rows,customerName);

    }


    /**
     * 修改客户
     * @return
     */
    @ApiOperation("新增或者修改用户")
    @PostMapping("customer/save")
    //Integer page, Integer rows, String codeOrName, Integer goodsTypeId
    public ServiceVO saveSupplier(
            @RequestParam(value = "customerId", required = false) Integer customerId,
            Customer customer
    ) {
        boolean returnResult =customerService.save(customerId,customer);//对代表成功

        if (returnResult ) {
            return new ServiceVO(100,"请求成功",null);
        }else {
            return new ServiceVO(500,"请求失败",null);
        }

    }
    /**
     * 删除供应商（支持批量删除）
     * http://localhost:8080/supplier/delete
     * @return ServiceVO
     */
    @PostMapping("customer/delete")
    public ServiceVO deleteSupplier(
            @RequestParam(value = "ids", required = false) String ids
    ) {
        boolean returnResult =customerService.delete(ids);//对代表成功
        if (returnResult) {
            return new ServiceVO(100,"请求成功",null);
        }else {
            return new ServiceVO(500,"请求失败",null);
        }

    }
}
