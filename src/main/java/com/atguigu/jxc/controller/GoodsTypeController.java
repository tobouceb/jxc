package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsTypeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @description 商品类别控制器
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 查询所有商品类别
     * @return easyui要求的JSON格式字符串
     */
    @PostMapping("/loadGoodsType")
    @RequiresPermissions(value={"商品管理","进货入库","退货出库","销售出库","客户退货","当前库存查询","商品报损","商品报溢","商品采购统计"},logical = Logical.OR)//shino安全框架权限注解
    public ArrayList<Object> loadGoodsType() {
        return goodsTypeService.loadGoodsType();
    }

    /**
     * 新增商品类别
     * @return easyui要求的JSON格式字符串
     */
    @PostMapping("/save")
    //String  goodsTypeName,Integer  pId
    public ServiceVO saveGoodsType(
//            @RequestParam(value = "goodsTypeName", required = false) String  goodsTypeName,
//            @RequestParam(value = "pId", required = false) Integer pId
            GoodsType goodsType
            ) {
        int pId = goodsType.getPId();
        switch (pId){
            case 1:  goodsType.setGoodsTypeState(1); //判断pid是否为1，为1就是根节点下子节点
            default:   goodsType.setGoodsTypeState(0);//0代表是叶子节点，1代表是非叶节点
        }
        Boolean b= goodsTypeService.saveGoodsType(goodsType);
        if (b) {
            return new ServiceVO(100,"成功",null);
        }else {
            return new ServiceVO(500,"失败",null);
        }

    }

    /**
     * 删除供应商（支持批量删除）
     * http://localhost:8080/supplier/delete
     * @return ServiceVO
     */
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(
            @RequestParam(value = "goodsTypeId", required = false) Integer  goodsTypeId

    ) {
        boolean returnResult =goodsTypeService.delete(goodsTypeId);//对代表成功

        if (returnResult) {
            return new ServiceVO(100,"请求成功",null);
        }else {
            return new ServiceVO(500,"请求失败",null);
        }

    }

}
