package com.atguigu.jxc.controller;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UnitController {
    @Autowired
    UnitDao unitDao;
    /**
     * 查询商品基本单位
     * @return json
     */
    @PostMapping("/unit/list")
    //Integer page, Integer rows, String  customerName
    public Map<String,Object> unitList() {
        List<Unit> units = unitDao.selectUnitList();
        Map<String, Object> resultPage =new HashMap<>();
        resultPage.put("rows",units);
        return resultPage;
    }
}
