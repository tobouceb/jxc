package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import com.github.pagehelper.PageHelper;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController

public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/goods/listInventory")
    //Integer page, Integer rows, String codeOrName, Integer goodsTypeId
    public Map<String,Object> listInventory(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "rows", required = false) Integer rows,
            @RequestParam(value = "codeOrName",required = false) String codeOrName,
            @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId
            ) {
        return goodsService.getGoodsInventoryPage(page,rows,codeOrName,goodsTypeId);

    }

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/goods/list")
    //Integer page, Integer rows, String codeOrName, Integer goodsTypeId
    public Map<String,Object> list(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "rows", required = false) Integer rows,
            @RequestParam(value = "codeOrName",required = false) String goodsName,
            @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId
    ) {
        return goodsService.getGoodsPage(page,rows,goodsName,goodsTypeId);
    }


    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/goods/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/goods/save")
    public ServiceVO saveOrUpdate(Goods goods ) {
        goodsService.saveGoods(goods);
        return new ServiceVO(100,"请求成功",null);
    }

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/goods/delete")
    public ServiceVO saveOrUpdate(@RequestParam(value = "goodsId", required = false) Integer goodsId) {
        goodsService.delete(goodsId);
        return new ServiceVO(100,"请求成功",null);
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "rows", required = false) Integer rows,
            @RequestParam(value = "nameOrCode",required = false) String nameOrCode
    ) {

        return goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
    }

    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "rows", required = false) Integer rows,
            @RequestParam(value = "nameOrCode",required = false) String nameOrCode
    ) {
        return goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
    }

    /**
     * 添加商品期初库存,修改数量或成本价
     * Integer goodsId,Integer inventoryQuantity,double purchasingPrice
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @PostMapping("/goods/saveStock")
    public ServiceVO saveStock(
            @RequestParam(value = "goodsId", required = false) Integer goodsId,
            @RequestParam(value = "inventoryQuantity", required = false) Integer inventoryQuantity,
            @RequestParam(value = "purchasingPrice",required = false) double purchasingPrice
    ) {
        goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO(100,"请求成功",null);     }

    /**
     * 删除商品库存
     * （要判断商品状态 入库、有进货和销售单据的不能删除）
     * http://localhost:8080/goods/deleteStock
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/goods/deleteStock")
    public ServiceVO deleteStock(
            @RequestParam(value = "goodsId", required = false) Integer goodsId
    ) {
        boolean b = goodsService.deleteStock(goodsId);
        if (b ) {
            return new ServiceVO(100,"请求成功",null);
        }else {
            return new ServiceVO(200, "请求失败", null);
        }
    }

    /**
     * 查询库存报警商品信息
     * @return
     */

}
