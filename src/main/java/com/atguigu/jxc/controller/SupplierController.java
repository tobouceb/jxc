package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.ReturnList;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 供应商管理Controller
 */
@RestController
public class SupplierController {
    @Autowired
    SupplierService supplierService;

    /**
     * 分页查询商品库存信息
     * Integer page（当前页数）, Integer rows（每页显示的记录数）, String supplierName
     * @param page 当前页
     * @param rows 每页显示条数
     * @return
     */
    @PostMapping("/supplier/list")
    //Integer page, Integer rows, String codeOrName, Integer goodsTypeId
    public Map<String,Object> supplierPage(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "rows", required = false) Integer rows,
            @RequestParam(value = "supplierName",required = false) String supplierName
    ) {
        return supplierService.pagelistSupplier(page,rows,supplierName);
    }

    /**
     * 新增供应商
     * http://localhost:8080/supplier/save?supplierId=1
     * @return
     */
    @ApiOperation("新增供应商")
    @PostMapping("supplier/save")
    //Integer page, Integer rows, String codeOrName, Integer goodsTypeId
    public ServiceVO saveSupplier(
            @RequestParam(value = "supplierId", required = false) Integer supplierId,
             Supplier supplier
    ) {
        boolean returnResult =supplierService.saveSupplier(supplierId,supplier);//对代表成功
        if (returnResult ) {
            return new ServiceVO(100,"请求成功",null);
        }else {
            return new ServiceVO(500,"请求失败",null);
        }

    }

    /**
     * 删除供应商（支持批量删除）
     * http://localhost:8080/supplier/delete
     * @return ServiceVO
     */
    @PostMapping("supplier/delete")
    public ServiceVO deleteSupplier(
            @RequestParam(value = "ids", required = false) String ids

    ) {
        boolean returnResult =supplierService.deleteSupplier(ids);//对代表成功
        if (returnResult) {
            return new ServiceVO(100,"请求成功",null);
        }else {
            return new ServiceVO(500,"请求失败",null);
        }

    }
}
