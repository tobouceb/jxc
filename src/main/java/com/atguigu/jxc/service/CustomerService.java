package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface CustomerService {
    Map<String, Object> getcustomerPage(Integer page, Integer rows, String customerName);

    boolean save(Integer customerId, Customer customer);

    boolean delete(String ids);
}
