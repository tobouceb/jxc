package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    /**
     * 查询商品库存
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsInventoryPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        List<Goods> goodsPage1 = goodsDao.getGoodsInventoryPage(page-1, rows, codeOrName, goodsTypeId);

        Map<String, Object> goodsPage =new HashMap<>();
        goodsPage.put("total",goodsPage1.size());
        goodsPage.put("rows",goodsPage1);

        return goodsPage;
    }

    /**
     * 查询全部商品分页
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsPage(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        List<Goods> goodsPage1 = goodsDao.getGoodsPage(page-1, rows, goodsName, goodsTypeId);

        Map<String, Object> goodsPage =new HashMap<>();
        goodsPage.put("total",goodsPage1.size());
        goodsPage.put("rows",goodsPage1);

        return goodsPage;
    }

    /**
     * 新增goods
     * @param goods
     * @return
     */
    @Override
    public boolean saveGoods(Goods goods) {
        Integer goodsId = goods.getGoodsId();
        if (goodsId !=null) {
            Goods oldgoods = goodsDao.getGoodsById(goodsId);
            goods.setInventoryQuantity(oldgoods.getInventoryQuantity());
            goods.setState(oldgoods.getState());
        }else {
            //这两个值前端没传递，自己填上，0是默认值
            goods.setInventoryQuantity(0);
            goods.setState(0);
        }
        goodsDao.saveorupdate(goods);
        return true;
    }

    /**
     * 删除
     * @param goodsId
     */
    @Override
    public void delete(Integer goodsId) {
        goodsDao.delete(goodsId);

    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        PageHelper.startPage(page,rows);
        List<Goods> goodsPage=goodsDao.getNoInventoryQuantity(nameOrCode);

        Map<String, Object> result =new HashMap<>();
        result.put("total",goodsPage.size());
        result.put("rows",goodsPage);
        return result;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        PageHelper.startPage(page,rows);
        List<Goods> goodsPage=goodsDao.getHasInventoryQuantity(nameOrCode);

        Map<String, Object> result =new HashMap<>();
        result.put("total",goodsPage.size());
        result.put("rows",goodsPage);
        return result;
    }
    /**
     * 添加商品期初库存,修改数量或成本价
     * Integer goodsId,Integer inventoryQuantity,double purchasingPrice
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock( goodsId,  inventoryQuantity, purchasingPrice);
    }

    /**
     * 删除库存
     * @param goodsId
     */
    @Override
    public boolean deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        Integer state = goods.getState();//0表示初始值,1表示已入库，2表示有进货或销售单据
        if (state!=0){
            return false;
        }
        goodsDao.delete(goodsId);
        return true;
    }

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }


}
