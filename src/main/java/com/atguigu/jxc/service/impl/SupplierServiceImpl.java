package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service

public class SupplierServiceImpl implements SupplierService {

    @Autowired
    SupplierDao supplierDao;

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> pagelistSupplier(Integer page, Integer rows, String supplierName) {
        List<Supplier> supplierPage = supplierDao.getSupplierPage(page - 1, rows, supplierName);

        Map<String, Object> goodsPage =new HashMap<>();
        goodsPage.put("total",supplierPage.size());
        goodsPage.put("rows",supplierPage);

        return goodsPage;

    }

    @Override
    public boolean saveSupplier(Integer supplierId,Supplier supplier) {
        try {
            if (supplierId == null) {
                //新增
                int id = supplierDao.saveSupplier(supplier);
            }else {
                //如果有id传递过来,就是修改
                supplierDao.updateSupplier(supplierId,supplier);
            }
            return true;
        } catch (Exception e) {

            return false;

        }
    }

    @Override
    public boolean deleteSupplier(String supplierId) {
        try {
            List<String> idList = Arrays.asList(supplierId.split(","));
            supplierDao.deleteSupplier(idList);
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
