package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerDao customerDao;
    @Override
    public Map<String, Object> getcustomerPage(Integer page, Integer rows, String customerName) {
        List<Customer> customerPage = customerDao.getCustomerPage(page-1, rows, customerName);

        Map<String, Object> resultPage =new HashMap<>();
        resultPage.put("total",customerPage.size());
        resultPage.put("rows",customerPage);

        return resultPage;
    }

    @Override
    public boolean save(Integer customerId, Customer customer) {
        try {
            if (customerId == null) {
                //新增
                int id = customerDao.saveCustomer(customer);
            }else {
                //如果有id传递过来,就是修改
                customerDao.updateCustomer(customerId,customer);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean delete(String ids) {
        try {
            List<String> idList = Arrays.asList(ids.split(","));
            customerDao.delete(idList);
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
