package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    /**
     * 分页查询 条件查询Supplier供应商表
     * @param
     * @return
     */
    Map<String, Object> pagelistSupplier(Integer page, Integer rows, String supplierName);

    /**
     * 新增或者修改 根据id判断
     * @param supplierId
     * @return
     */
    boolean saveSupplier(Integer supplierId, Supplier supplier);

    /**
     * 删除供应商 支持批量 根据id判断
     * @param supplierId
     * @return
     */
    boolean deleteSupplier(String supplierId);
}
