package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {

//查询商品库存
    Map<String, Object> getGoodsInventoryPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) ;

    ServiceVO getCode();

//查询商品分页
    Map<String, Object> getGoodsPage(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    boolean saveGoods(Goods goods);

    void delete(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    boolean deleteStock(Integer goodsId);
}
